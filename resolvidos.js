// Escreva uma função que receba um vetor como parâmetro e devolva outro, mas sem elementos repetidos.
const numerosVarios = [ 45, 765, 23, 76, 43, 65, 2345, 677, 5, 34, 22, 546, 77, 4355, 5667, 45, 56, 7, 4, 755, 45]

function removerDuplicados(array){
	const arraySemDuplicados = []
	for(let i = 0; i < array.length; i++){
			if(arraySemDuplicados.indexOf(array[i]) == -1){
					arraySemDuplicados.push(array[i])
			}
	}
	return arraySemDuplicados;
}

console.log(removerDuplicados(numerosVarios))

// Escreva uma função que receba um vetor como parâmetro e devolva outro, mas sem elementos repetidos COM ORDENAÇÃO

function removerDuplicados(array){
	const arraySemDuplicados = []
	for(let i = 0; i < array.length; i++){
			if(arraySemDuplicados.indexOf(array[i]) == -1){
					arraySemDuplicados.push(array[i])
			}
	}
	return arraySemDuplicados.sort(function(a, b){return a - b});
}

console.log(removerDuplicados(numerosVarios))

// Escreva uma função que busque um elemento em um vetor ordenado usando o algoritmo de busca binária.

const numerosOrdenados = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]

function buscaBinaria(array, valor) {
  let busca;
  let min = 0;
  let max = array.length - 1;	

  while(min <= max){
    busca = Math.floor((min + max) /2);
	if(array[busca] === valor)
	  return busca;
	else if(array[busca] < valor)
	  min = busca + 1;
	else
	  max = busca - 1;
   }
	console.log(busca);
}
buscaBinaria(numerosOrdenados, 20);

// Inverta a ordem de uma array de caracteres sem utilizar o método array.reverse().
// Resolva de duas formas: utilizando somente array.push() / utilizando array.pop() e array.splice()

const letras = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"]

const tamanho = letras.length;
const ultimoIndice = tamanho-1;
const letrasReverse = [];

for (let i = ultimoIndice; i >= 0; i--){
  console.log(letras[i]);  
  letrasReverse.push(letras[i]);
}
 console.log(letrasReverse);

function spliceReverse(array) {
  for (let i = 0; i < array.length; i++){
    let itemSaiu = array.pop();
    array.splice(i, 0, itemSaiu);
  }
  console.log(array);
}

spliceReverse(letras);