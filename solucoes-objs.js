const arr = [
  [
    ['nome', 'Helena'], ['sobrenome', 'Roitmann'], ['idade', 42], ['cargo', 'front-end']
  ],
  [
    ['nome', 'Nazaré'], ['sobrenome', 'Tedesco'], ['idade', 50 ], ['cargo', 'PO']
  ]
]

function teste(arr) {
	const arrayNova = []
	for(let item of arr){
		const obj = {}
		for(let oi of item) {
			obj[oi[0]] = oi[1]
		}
		arrayNova.push(obj)
	}
	return arrayNova
}

console.log(teste(arr))

/////////////////////

const obj = {
  especie: 'felino',
  nome: 'Satanás',
  peso: 6.66
}

function teste(obj){
	const arr = []
	for (let i in obj){
		const arrItem = []
		arrItem.push(i, obj[i])
		arr.push(arrItem)
	}
	return arr
}
console.log(teste(obj));

/////////////////////

const obj = {
  key: [4, 1, 8]
};

function teste(obj, key){
	// let sum = 0;
	// for (i of obj[key]){
	// 	sum += i
	// }
	// return sum

	return obj[key].reduce((atual, acum) => atual + acum , 0)
}

console.log(teste(obj, 'key'));

/////////////////////

const pessoa1 = {
  nome: 'Smithers',
  role: 'secretário'
};
const pessoa2 = {
  nome: 'Sr. Burns',
  role: 'chefe'
};

function teste(obj1, prop, obj2){
	obj1[prop] = obj2
}

teste(pessoa1, 'gerente', pessoa2)
console.log(pessoa1)
console.log(pessoa1.gerente)

/////////////////////

const person = {
  firstName: 'Maria',
  lastName: 'do Bairro'
};

function teste(obj){
	obj.fullName = obj.firstName + " " + obj.lastName
}
teste(person)
console.log(person.fullName);

/////////////////////

carro.passageiros = function(numeroPessoas){
	let totalPessoas = carro.quantidadePessoas + numeroPessoas;
	if (carro.quantidadePessoas === carro.assentos && totalPessoas >= carro.assentos){
			return `o carro já está lotado!`;
	}
	if (totalPessoas > carro.assentos) {
			return `Só cabe mais ${carro.assentos - carro.quantidadePessoas} pessoa(s)`;
	}
	carro.quantidadePessoas += numeroPessoas;
	return `Já temos ${carro.quantidadePessoas} pessoas no carro!`;
}

	console.log(carro.passageiros(2));
	console.log(carro.passageiros(2));
	console.log(carro.passageiros(2));
	